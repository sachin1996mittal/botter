var express = require('express');
var bodyParser = require('body-parser');
var request = require('request');
var cron = require('cron').CronJob;
var waterfall = require('async-waterfall');
var rp = require('request-promise');

var app = express();
var PAGE_ACCESS_TOKEN = 'EAAD1ch9Nu9oBAA5SvYuV9r2PsloI9oDHbOiYsfyzwO4vjL396UqS63UXMRse6f4vXSP1ON3Gck98mA53eZAGvMf7GFHK0lrd6tcPq3wKGEgenVIsAz7pIvcMUfvJBfkFFiXA05H1pDBXvbDtIgAOS0sceKVhDhfUQxA9TJwZDZD';

var MongoClient = require('mongodb').MongoClient;
var mongo_db;
var url = 'mongodb://127.0.0.1/spider_chatbot';
MongoClient.connect(url, function(err, db) {
  console.log("Connected to server correctly.");
  mongo_db = db;
});
var jsonobj = [];

//var ObjectId = require('mongodb').ObjectID;



app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
// app.use(express.static('/'));
app.listen(80);


// Server frontpage
app.get('/', function (req, res) {
    res.send('This is TestBot Server2');
});


// Facebook Webhook
app.get('/webhook', function(req, res) {


    // console.log("webhook get");
  if (req.query['hub.mode'] === 'subscribe' &&
      req.query['hub.verify_token'] === 'DOITBIG') {
    console.log("Validating webhook");
    res.status(200).send(req.query['hub.challenge']);
  //res.send("Holaa");
  } else {
    console.error("Failed validation. Make sure the validation tokens match.");
    res.sendStatus(403);    
  }  
});

// Creates the endpoint for our webhook
app.post('/webhook', (req, res) => {
 
    let body = req.body;
 
    if (body.object === 'page') {
 
        // Iterates over each entry - there may be multiple if batched
        body.entry.forEach(function(entry) {
 
            // Gets the message. entry.messaging is an array, but
            // will only ever contain one message, so we get index 0
            let webhook_event = entry.messaging[0];
            console.log(webhook_event);
 
            // Get the sender PSID
            let sender_psid = webhook_event.sender.id;
            let recipient_psid = webhook_event.recipient.id;
            
            console.log('Sender PSID: ' + sender_psid+" Recipient id=>"+recipient_psid);
 
            // Check if the event is a message or postback and
            // pass the event to the appropriate handler function
            if (webhook_event.message) {

                console.log('message--> '+webhook_event.message.text);
                senderAction(sender_psid,'typing_on');
                handleMessage(sender_psid, webhook_event.message);
            } else if (webhook_event.postback) {
                console.log('postback-->'+webhook_event.postback.text);
                senderAction(sender_psid,'typing_on');
                handlePostback(sender_psid, webhook_event.postback);
            }
        });
 
        // Returns a '200 OK' response to all requests
        res.status(200).send('EVENT_RECEIVED');
    } else {
        // Returns a '404 Not Found' if event is not from a page subscription
        res.sendStatus(404);
    }
 
});


// var cr = cron.schedule("2 * * * *", function() {
//       console.log("running a task every minute");
//     });

// cr.start();


//Greeting Text that is shown on get starting page
request({
    url:'https://graph.facebook.com/v3.0/me/thread_settings',
        qs: {access_token: PAGE_ACCESS_TOKEN},
    method: 'POST',
    json: {
        setting_type: 'greeting',
        greeting:
        {
            text: 'हैलो {{user_first_name}}\nमैं अमरउजाला न्यूज़ मेस्सेंजर हु।  यहाँ पर आप मुझसे अपनी रूचि अनुसार प्रशन पूछ सकते है और अपने पसंदीदा विषय को सब्सक्राइब भी कर सकते है'
        }

    }
    }, function(error, response, body) {
        if (error) {
            console.log('172 Error sending message: ', error);
        } else if (!error && response.statusCode == 200) {
            
        }
    });



//Persistent Menu to be shown at the below of chat screen
request({
        url: 'https://graph.facebook.com/v3.0/me/thread_settings',
        qs: {access_token: PAGE_ACCESS_TOKEN},
        method: 'POST',
        json: {
            "locale":"default",
            setting_type: "call_to_actions",            
            call_to_actions: [
            {   type: "postback",
                title: "TOP STORIES",
                payload: "top_stories"
            },
            {
                type: "postback",
                title: "MENU",
                payload: "menu"
            },
            {
                type: "nested",
                title: "SETTINGS",
                call_to_actions : [
                    {
                      title:"Unsubscribe",
                      type:"postback",
                      payload:"unsubscribe"
                    },
                    {
                      title:"check subscriptions",
                      type:"postback",
                      payload:"check_subscriptions"
                    },
                    {
                      title:"Share this bot",
                      type:"postback",
                      payload:"share_bot"
                    }
                  ]
            }   
            ]
        }
    }, function(error, response, body) {
        if (error) {
            console.log('172 Error sending message: ', error);
        } else if (!error && response.statusCode == 200) {
            // console.log("174 + "+body);
        }
    });




//to set Get Started Button's payload
request({
        url: 'https://graph.facebook.com/v3.0/me/messages',
        qs: {access_token: PAGE_ACCESS_TOKEN},
        method: 'POST',
        json: {
            setting_type: "call_to_actions",
            thread_state: "new_thread",
            call_to_actions: [{
                payload: "Get Started"
            }]
        }
    }, function(error, response, body) {
        if (error) {
            console.log('Error sending message: ', error);
        } else if (!error && response.statusCode == 200) {
            
        }
    });

//not in use
// const after_welcome_message = (text) => {
//     return {
//         "attachment":{
//             "type":"template",
//             "payload":{
//                 "template_type":"button",
//                 "text": text,
//                 "buttons":[
//                     {
//                         "type":"postback",
//                         "title":"MENU",
//                         "payload":"menu"
//                     }
//                 ]
//             }
//         }
//     }
// }


//shows that bot is typing ...
function senderAction(sender_psid, action){
    console.log('action-'+action+"sender_psid"+sender_psid);
    request({
        "uri": "https://graph.facebook.com/v3.0/me/messages",
        "qs": { "access_token": PAGE_ACCESS_TOKEN },
        "method": "POST",
        "json": {
            "recipient":{"id":sender_psid},
            "sender_action": "typing_on"
        }
    },function(error,response,body){
        if(error){
            console.log(response.sender_psid);
        }
    });

}


//send a normal text without any template 
function PlainMessage(sender_psid, text) {
    return rp({
        url: 'https://graph.facebook.com/v3.0/me/messages',
        qs: {access_token: PAGE_ACCESS_TOKEN},

        method: 'POST',
        
        json: {
            recipient: {id: sender_psid},
            
            message: text,
        }
    }).then(function(){
        console.log("Ok");
    })
    .catch(function(error, response, body) {
             console.log('Error sending message: ', error);
    });
};


// Sends response messages via the Send API
const callSendAPI = (sender_psid, response, cb = null) => {
    // Construct the message body
    let request_body = {
        "recipient": {"id": sender_psid},
        "message": response,
    };


     return rp({
        "uri": "https://graph.facebook.com/v3.0/me/messages",
        "qs": { "access_token": PAGE_ACCESS_TOKEN },
        "method": "POST",
        "json": request_body
    }, (err, res, body) => {
        if (!err) {
            if(cb){
                cb();
            }
        } else {
            console.error("Unable to send message:" + err);
        }
    });
}



//template that ask wether user wants to visit kavya?
const ask_for_kavya = (text) => {
    return {
        "attachment":{
            "type":"template",
            "payload":{
                "template_type":"button",
                "text": text,
                "buttons":[
                    {
                        "type":"web_url",
                        "url": "https://www.amarujala.com/kavya",
                        "title":"YES",
                        //"payload":"yes_for_kavya"
                    },
                    {
                        "type":"postback",
                        "title":"NO",
                        "payload":"no_for_kavya"
                    }
                ]
            }
        }
    }
}

//template that ask wether user wants to visit e-paper?
const ask_for_e_paper = (text) => {
    return {
        "attachment":{
            "type":"template",
            "payload":{
                "template_type":"button",
                "text": text,
                "buttons":[
                    {
                        "type":"web_url",
                        "url": "http://epaper.amarujala.com/?format=img&p=today",
                        "title":"YES",                        
                    },
                    {
                        "type":"postback",
                        "title":"NO",
                        "payload":"no_for_e_paper"
                    }
                ]
            }
        }
    }
}


//template that ask wether user want to subscribe after showing the news
const ask_subscription = (text,payload_value) => {
    return {
        "attachment":{
            "type":"template",
            "payload":{
                "template_type":"button",
                "text": text,
                "buttons":[
                    {
                        "type":"postback",
                        "title":"SUBSCRIBE",
                        "payload":payload_value+'_subscribe',
                    }
                ]
            }
        }
    }
}


//template that ask if u want subscribe?
const ask_in_or_out = (text) => {
    return {
        "attachment":{
            "type":"template",
            "payload":{
                "template_type":"button",
                "text": text,
                "buttons":[
                    {
                        "type":"postback",
                        "title":"YES I'm in                                      ",
                        "payload":"subscription_in",
                    },
                    {
                        "type":"postback",             
                        "title":"No I'm out                                     ",
                        "payload":"subscription_out",
                    }
                ]
            }
        }
    }
}



// handles messages that are hyperparameters
function search_news(sender_psid, typed_word)
{

    // console.log("952");
     PlainMessage(sender_psid,  {text: "प्रतीक्षा करें..." });



    request({
        url: 'http://api.amarujala.com/v1/search',
        qs: {keywords: typed_word},
        method: 'GET'
    },function(error, response, body){
        if(error)
           { console.log("797 error");
        console.log(error);
        }
        else
        {
            var res = JSON.parse(body);

            //console.log(JSON.stringify(res));

            var store = "{\"news\":[]}";
           //console.log("941",res['news'].length);

            if(res['news'].length != 0)
            {


            jsonobj = [];
            for (i=0;i<8 && i<res['news'].length;i++)
            {
                

                //If else needed to avoid replace error in images, as some images not present replaced with au logo
                if(res['news'][i]['image'])
                    {   jsonobj.push({
                            "title": res['news'][i]['Hindi-Headline'],
                            "subtitle": res['news'][i]['News-Synopsis'],
                            "image_url": res['news'][i]['image'].replace('250x250','500x500'),
                            "default_action": {
                                "type": "web_url",
                                "url": res['news'][i]['Share_URL']+'?src=fbchatbot'
                            },
                            "buttons": [{
                                "type": "web_url",
                                "url": res['news'][i]['Share_URL']+'?src=fbchatbot',
                                "title": "Quick Brief"

                            },
                            {
                                "type": "web_url",
                                "url": res['news'][i]['Share_URL']+'?src=fbchatbot',
                                "title": "Full Story"
                            }]
                    });
            }

            else
            {
                jsonobj.push({
                            "title": res['news'][i]['Hindi-Headline'],
                            "subtitle": res['news'][i]['News-Synopsis'],
                            "image_url": "http://chatbot.amarujala.com/assets/chatbot-au.jpg",
                            "default_action": {
                                "type": "web_url",
                                "url": res['news'][i]['Share_URL']+'?src=fbchatbot'
                            },
                            "buttons": [{
                                "type": "web_url",
                                "url": res['news'][i]['Share_URL']+'?src=fbchatbot',
                                "title": "Quick Brief"

                            },
                            {
                                "type": "web_url",
                                "url": res['news'][i]['Share_URL']+'?src=fbchatbot',
                                "title": "Full Story"
                            }]
                    });

            }

            }


            

            message = {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "generic",
                         "elements": jsonobj
                    }
                }
            }

            callSendAPI(sender_psid, message);

            return true;
        }

        else
            PlainMessage(sender_psid ,  {text: "क्षमा करें, इस विषय पर कोई अपडेट नहीं है। पुनः प्रयास करें।" });

        }


    });
}





//to show the news in list view ... not in use right now
const show_news_listView = (text,payload_text) => {
    return {
        "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "list",
                        "top_element_style": "LARGE",
                         "elements": text,
                         "buttons": [{
                                "type":"postback",
                                "title": "MORE",
                                "payload": payload_text + "_more"
                            }
                            ]

                        }
                    
                    }

        }
}


//to show the news in generic view 
const show_news_genericView = (text,payload_text) => {
    return {
        "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "generic",                        
                         "elements": text,                   

                        }
                    
                    }

        }
}



//template to ask at which tym user wants to have news
const select_time = (text) =>{

    return {

        "text" : text,
        "quick_replies" : [

          
          {
            
            "content_type":"text",
            "title":"Once a day",
            "payload":"payload1",
            "image_url":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQP5zpt3KFRuhM-cey6yVMQsDl3JEI5DBqbxfRuSjETGuAIgGQA9A"
          },
          {
            
            "content_type":"text",
            "title":"Thrice a day",
            "payload":"payload1",
            "image_url":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQP5zpt3KFRuhM-cey6yVMQsDl3JEI5DBqbxfRuSjETGuAIgGQA9A"
          }, 
          {
            
            "content_type":"text",
            "title":"Enter time?",
            "payload":"payload1",
            "image_url":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQP5zpt3KFRuhM-cey6yVMQsDl3JEI5DBqbxfRuSjETGuAIgGQA9A"
          }, 
          {
            
            "content_type":"text",
            "title":"5am | 9pm | 10pm",
            "payload":"payload1",
            "image_url":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQP5zpt3KFRuhM-cey6yVMQsDl3JEI5DBqbxfRuSjETGuAIgGQA9A"
          }
        ]
      }

}



//results in showing every type of news
function showing_every_type_of_news  (sender_psid, text,num)  {
    if (text=='top_st')
    {
        request({
        sender_psid: sender_psid,
        url: 'http://api.amarujala.com/v1/home-page-sections-news',
        method: 'GET'
    },function(error, response, body){
        if(error)
            console.log("589 error");
        else
        {
            var res = JSON.parse(body);
            //console.log(body);
            every_news = [];
            num_end=num+8;
            for (i=num;i<num_end;i++)
            {
                every_news.push({
                            "title": res['news'][i]['Hindi-Headline'],
                            "image_url": res['news'][i]['image'].replace('250x250','500x500'),
                            "default_action": {
                                "type": "web_url",
                                "url": res['news'][i]['Share_URL']+'?src=fbchatbot'
                            },
                            "buttons": [{
                                "type": "web_url",
                                "url": res['news'][i]['Share_URL']+'?src=fbchatbot',
                                "title": "READ"
                            }]
                    });

            }

            response = show_news_genericView(every_news,text);
            callSendAPI(sender_psid, response).then(function(){
                response = ask_subscription("subscribe us for free and we will send you news in your inbox automativally.\nAlready subscribed then ignore it.\nDont worry! its easy and free",text);
                callSendAPI(sender_psid,response);

            })

            

            return true;

        }

    });
    }

    else if (text=='tranding-stories')
    {
        request({
        sender_psid: sender_psid,
        url: 'http://api.amarujala.com/v1/'+text,
        method: 'GET'
    },function(error, response, body){
        if(error)
            console.log("589 error");
        else
        {
            var res = JSON.parse(body);
            //console.log(body);
            every_news = [];
            num_end=num+8;
            for (i=num;i<num_end;i++)
            {
                every_news.push({
                            "title": res['news'][i]['Hindi-Headline'],
                            "image_url": res['news'][i]['image'].replace('250x250','500x500'),
                            "default_action": {
                                "type": "web_url",
                                "url": res['news'][i]['Share_URL']+'?src=fbchatbot'
                            },
                            "buttons": [{
                                "type": "web_url",
                                "url": res['news'][i]['Share_URL']+'?src=fbchatbot',
                                "title": "READ"
                            }]
                    });

            }

            response = show_news_genericView(every_news,text);
            callSendAPI(sender_psid, response).then(function(){
                response = ask_subscription("subscribe us for free and we will send you news in your inbox automativally.\nAlready subscribed then ignore it.\nDont worry! its easy and free",text);
                callSendAPI(sender_psid,response);

            });

            return true;

        }

    });

    }
    
    else if (text=='recent')
    {
        request({
        sender_psid: sender_psid,
        url: 'http://api.amarujala.com/v1/recentnews/',
        method: 'GET'
    },function(error, response, body){
        if(error)
            console.log("589 error");
        else
        {
            var res = JSON.parse(body);
            //console.log(body);
            every_news = [];
            num_end=num+8;
            for (i=num;i<num_end;i++)
            {
                every_news.push({
                            "title": res['news'][i]['Hindi-Headline'],
                            "image_url": res['news'][i]['image'].replace('250x250','500x500'),
                            "default_action": {
                                "type": "web_url",
                                "url": res['news'][i]['Share_URL']+'?src=fbchatbot'
                            },
                            "buttons": [{
                                "type": "web_url",
                                "url": res['news'][i]['Share_URL']+'?src=fbchatbot',
                                "title": "READ"
                            }]
                    });

            }

            response = show_news_genericView(every_news,text);
            callSendAPI(sender_psid, response).then(function(){
                response = ask_subscription("subscribe us for free and we will send you news in your inbox automativally.\nAlready subscribed then ignore it.\nDont worry! its easy and free",text);
                callSendAPI(sender_psid,response);

            })

            return true;

        }

    });

    } 

    else 
    {
        request({
        sender_psid: sender_psid,
        url: 'http://api.amarujala.com/v1/recentnews/category/'+text,

        //http://api.amarujala.com/v1/recentnews/category/india-news
        method: 'GET'
    },function(error, response, body){
        if(error)
            console.log("589 error");
        else
        {
            var res = JSON.parse(body);
            //console.log(body);
            every_news = [];
            num_end=num+8;
            for (i=num;i<num_end;i++)
            {
                every_news.push({
                            "title": res['news'][i]['Hindi-Headline'],
                            "image_url": res['news'][i]['image'].replace('250x250','500x500'),
                            "default_action": {
                                "type": "web_url",
                                "url": res['news'][i]['Share_URL']+'?src=fbchatbot'
                            },
                            "buttons": [{
                                "type": "web_url",
                                "url": res['news'][i]['Share_URL']+'?src=fbchatbot',
                                "title": "READ"
                            }]
                    });

            }

            response = show_news_genericView(every_news,text);
            callSendAPI(sender_psid, response).then(function(){
                response = ask_subscription("subscribe us for free and we will send you news in your inbox automativally.\nAlready subscribed then ignore it.\nDont worry! its easy and free",text);
                callSendAPI(sender_psid,response);

            })

            return true;

        }

    });

    } 

}

 




//hande the subscription:
function subscribe_any_type_of_news(sender_psid, text)
{
    console.log("payload==" + text);
    PlainMessage(sender_psid,{text:"SUBSCRIBED!!\nNow you will be getting the updates news in your inbox for free.\nYou can any time unsubscribe this service through our settings menu. "});
}


//to show the menu i.e the type of news available
const show_menu_list = (text) =>{

    return {

        "text" : text,

        //"type" : "postback",
        "quick_replies" : [

          
          {
            
            "content_type":"text",
            "title":"TOP STORIES",
            "payload":"payload1",
            "image_url":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT3xXSn00Df_mxgk63sTHexuMwL_UXAb3CYlKeVwJuO4EFOIMGeBQ"
          },
          {
            
            "content_type":"text",
            "title":"TRENDING",
            "payload":"payload1",
            "image_url":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTZgIrsSMCwKrbNM5H4sr-HbWsKNI-JiiNK8vNQxrM2c42bq5im"
          }, 
          {
            
            "content_type":"text",
            "title":"LATEST",
            "payload":"payload1",
            "image_url":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRiONFZQmobopXnYXrp7_I-VyfW3Dd6EU9k1krDZYWoL7AkLNANhA"
          }, 
          {
            
            "content_type":"text",
            "title":"INDIA",
            "payload":"payload1",
            "image_url":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTexhRpcngEqBMrJLI6BXUWsyrN1d12LiPwndclKaWUcxhINCcKCA"
          }, 
          {
            
            "content_type":"text",
            "title":"ENTERTAINMENT",
            "payload":"payload1",
            "image_url":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQkvmHHKIQ-uti8lGs80_y_n9CxRIbOjA_4G9ITJ54mhjUHHk6PTQ"
          }, 
          {
            
            "content_type":"text",
            "title":"CRICKET",
            "payload":"payload1",
            "image_url":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0p0NL53skVgPfoo-yuFscJQjknjUzdpL2igbBCSL861Kjgm4-fg"
          }, 
          {
            
            "content_type":"text",
            "title":"PHOTOS",
            "payload":"payload1",
            "image_url":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT4llNXZlNE9eC3F8QoEyrYQZg6KcoRDghlsWg4pIXo3RuvVcNteA"
          },
          {
            
            "content_type":"text",
            "title":"VIDEOS",
            "payload":"payload1",
            "image_url":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTi0AH_v7F0XlXLeW8yvsM3aNZZsjhWWe-HKgvFDYw22gc7kjEdxA"
          },
          {
            
            "content_type":"text",
            "title":"JOB",
            "payload":"payload1",
            "image_url":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQvMAKCfWooM4ckj96sldpXZX5OdmGaHxKnkrzsR-GHgGXIkdMG"
          },
          {
            
            "content_type":"text",
            "title":"KAVYA",
            "payload":"payload1",
            "image_url":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS_tbNPbTHKABmX8FEoxB35YN_mE5nLO83FhHyFkbltr79V6GmU"
          },
          {
            
            "content_type":"text",
            "title":"E-paper",
            "payload":"payload1",
            "image_url":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSKDqcwZM9vGXM-pItkPbGXPBP0rdkJV-bQDL2auywUqalnjE1r"
          }
        ]
      }

}

//shows the respective news
function top_stories (sender_psid,more_value)
{
    showing_every_type_of_news(sender_psid,"top_st",more_value)
}

//shows the respective news
function trending (sender_psid,more_value)
{
    showing_every_type_of_news(sender_psid,"tranding-stories",more_value)

}

//shows the respective news
function latest (sender_psid,more_value)
{
    showing_every_type_of_news(sender_psid,"recent",more_value)
   
}

//shows the respective news
function india (sender_psid,more_value)
{
    showing_every_type_of_news(sender_psid,"india-news",more_value)
   
}


//shows the respective news
function entertainment (sender_psid,more_value)
{
    showing_every_type_of_news(sender_psid,"entertainment",more_value)
   
}

//shows the respective news
function cricket (sender_psid,more_value)
{
    showing_every_type_of_news(sender_psid,"cricket",more_value)
   
}

//shows the respective news
function photos (sender_psid,more_value)
{
    showing_every_type_of_news(sender_psid,"photo-gallery",more_value)
   
}

//shows the respective news
function videos (sender_psid,more_value)
{
    showing_every_type_of_news(sender_psid,"video",more_value)
   
}

//shows the respective news
function jobs (sender_psid,more_value)
{
    showing_every_type_of_news(sender_psid,"jobs",more_value)
   
}


//shows the respective news
function astrology (sender_psid,more_value)
{
    showing_every_type_of_news(sender_psid,"astrology",more_value)
}

//takes user to kavya
function kavya (sender_psid)
{
    response = ask_for_kavya('kya aap kavya ki website pr jaana chahte h');
    callSendAPI(sender_psid,response);
   
}

//takes user to e-paper
function e_paper (sender_psid)
{
    response = ask_for_e_paper('kya aap e-paper padhna chahte h?');
    callSendAPI(sender_psid,response);
   
}





// hndles postback events
const handlePostback = (sender_psid, received_postback) => {
    let response;
    
    // Get the payload for the postback
    let payload = received_postback.payload;
    //var a=1;
    console.log("Inside handlePostback sender PSID->"+sender_psid+" ==received payload"+payload.text);
    if(payload === "GET_STARTED" || payload === "Get Started"){
        PlainMessage(sender_psid, {text: "मैं अमरउजाला न्यूज़ मेस्सेंजर हु।  यहाँ पर आप मुझसे अपनी रूचि अनुसार प्रशन पूछ सकते है और अपने पसंदीदा विषय को सब्सक्राइब भी कर सकते है."} )
        .then(function(){
            response = show_menu_list("choose:");
            callSendAPI(sender_psid, response);
        });
        // waterfall([
           
        //     function(callback){
        //         PlainMessage(sender_psid, {text: "मैं अमरउजाला न्यूज़ मेस्सेंजर हु।  यहाँ पर आप मुझसे अपनी रूचि अनुसार प्रशन पूछ सकते है और अपने पसंदीदा विषय को सब्सक्राइब भी कर सकते है."} );
        //         console.log('One');
        //         //var result= JSON.parse(body);
        //         callback(null, 'first message complete');
                
        //     },
        //     function (arg, callback){
        //         console.log('two');  
        //         //PlainMessage(sender_psid, {text: "chaliye shuru krte h"} );     
        //         //response = after_welcome_message('सुरु करने के लिया Menu पर click करे या चैट बॉक्स मई अपने पसंद का विषय डेल. ');
        //         response = show_menu_list("apni manpasand ruchi ko chune")
        //         //setTimeout(function(),callSendAPI(sender_psid,response);
        //         setTimeout(function(){callSendAPI(sender_psid, response);},1000)
        //         callback(null, 'done');
        //     }
            
        //     ], function (err, result) 
        // {
        //     if(err)
        //         console.log("633", err.message);
        //     else
        //         console.log(result);
        // });
    }

    if(payload === "top_stories"){
        waterfall([
           
            function(callback){
                PlainMessage(sender_psid, {text: "Top stories"} );
                console.log('One');
                //var result= JSON.parse(body);
                callback(null, 'first message complete');
                
            },function(arg, callback){
                
                console.log('two');
                top_stories(sender_psid,0);
                callback(null, 'first message complete');
                
            }
            ], function (err, result) 
        {
            if(err)
                console.log("633", err.message);
            else
                console.log(result);
        });


    }

    if(payload === "menu"){
        //PlainMessage(sender_psid, {text: "Choose:"} );
        response = show_menu_list("choose:");
        callSendAPI(sender_psid,response);
    }

    if(payload === 'top_st_more'){
        //console.log(count)
        top_stories(sender_psid,5);   
        //count=count+5;   
        return true;    
    }

    if(payload === 'tranding-stories_more'){
        //console.log(count)
        trending(sender_psid,5);   
        //count=count+5;   
        return true;    
    }

    if(payload === 'recent_more'){
        //console.log(count)
        latest(sender_psid,5);   
        //count=count+5;   
        return true;    
    }

    if(payload === 'india-news_more'){
        //console.log(count)
        india(sender_psid,5);   
        //count=count+5;   
        return true;    
    }

    if(payload === 'entertainment_more'){
        //console.log(count)
        entertainment(sender_psid,5);   
        //count=count+5;   
        return true;    
    }

    if(payload === 'cricket_more'){
        //console.log(count)
        cricket(sender_psid,5);   
        //count=count+5;   
        return true;    
    }

    if(payload === 'photo-gallery_more'){
        //console.log(count)
        photos(sender_psid,5);   
        //count=count+5;   
        return true;    
    }

    if(payload === 'video_more'){
        //console.log(count)
        videos(sender_psid,5);   
        //count=count+5;   
        return true;    
    }

    if(payload === 'jobs_more'){
        //console.log(count)
        jobs(sender_psid,5);   
        //count=count+5;   
        return true;    
    }

    if(payload === 'astrology_more'){
        //console.log(count)
        astrology(sender_psid,5);   
        //count=count+5;   
        return true;    
    }

    if(payload === 'no_for_kavya'){
        PlainMessage(sender_psid, {text: "Its ok, may be next tym. here is the menu"} );
        response = show_menu_list("choose:");
        callSendAPI(sender_psid,response);
           
        return true;    
    }

    //no_for_e_paper
    if(payload === 'no_for_e_paper'){
        PlainMessage(sender_psid, {text: "Its ok, may be next tym. here is the menu"} );
        response = show_menu_list("choose:");
        callSendAPI(sender_psid,response);
           
        return true;    
    }

    if(payload === 'top_st_subscribe'){
        
        //top_stories(sender_psid,5);   
          
        response = ask_in_or_out("We will send you the topics at your suitable timings.\nAre you ok with that");
        callSendAPI(sender_psid, response);
        return true;    
    }

    if(payload === 'tranding-stories_subscribe'){
        subscribe_any_type_of_news(sender_psid, payload);
        return true;    
    }

    if(payload === 'recent_subscribe'){
        subscribe_any_type_of_news(sender_psid, payload);  
        return true;    
    }

    if(payload === 'india-news_subscribe'){
        subscribe_any_type_of_news(sender_psid, payload);   
        return true;    
    }

    if(payload === 'entertainment_subscribe'){
        subscribe_any_type_of_news(sender_psid, payload);   
        return true;    
    }

    if(payload === 'cricket_subscribe'){
        subscribe_any_type_of_news(sender_psid, payload);   
        return true;    
    }

    if(payload === 'photo-gallery_subscribe'){
        subscribe_any_type_of_news(sender_psid, payload);  
        return true;    
    }

    if(payload === 'video_subscribe'){
        subscribe_any_type_of_news(sender_psid, payload);   
        return true;    
    }

    if(payload === 'jobs_subscribe'){
        subscribe_any_type_of_news(sender_psid, payload);   
        return true;    
    }

    if(payload === 'astrology_subscribe'){
        subscribe_any_type_of_news(sender_psid, payload);   
        return true;    
    }

    if(payload === 'subscription_in'){
        // PlainMessage(sender_psid, {text: "OK!\nSelect the suitable set for yourself"} ).then(function(){
        //     response = select_time("here are topic:");
        //     callSendAPI(sender_psid, response);
        // });   
        response = select_time("at what timings will you like to have news?");
        callSendAPI(sender_psid, response);
        return true;    
    }

    if(payload === 'subscription_out'){
        PlainMessage(sender_psid, {text: "OK!\nyou can any time subscribe us"} ).then(function(){
            response = show_menu_list("here are topic:");
            callSendAPI(sender_psid, response);
        });
        // response = show_menu_list("choose:");
        // callSendAPI(sender_psid,response);
        
        return true;    
    }

    // if (payload === 'settings'){
    //     settings_clicked();
    // }
   
}


// Handles messages events
const handleMessage = (sender_psid, received_message) => {
    let response;
    console.log("Inside Handle Message sender PSID->"+sender_psid+" ==received message"+received_message.text);
    if (received_message.text) {
        if(received_message.text == 'menu'){
            waterfall([
            function(callback){
                PlainMessage(sender_psid,{text: "Here you are with our menu."});
                callback(null,'first message sent');
            },
            function(arg, callback){
                response = askTemplate("choose here");
                setTimeout(function(){callSendAPI(sender_psid, response);},1000);
                callback(null,'first','second');
            }

            ], function(err, err1, result){
                if(err)
                    console.log("633",err);
                else
                    console.log("result");
            });
            
        }
    


    if(received_message.text.toLowerCase() == 'hi' || received_message.text.toLowerCase() == 'hello' || received_message.text.toLowerCase() == 'hey'){
        PlainMessage(sender_psid,{text:"Hey! Try Our chatbot. click menu"})           
    }

    if(received_message.text == 'TOP STORIES'){
        top_stories(sender_psid,0);            
    }


    if(received_message.text == 'TRENDING'){
        trending(sender_psid,0);            
    }

    if(received_message.text == 'LATEST'){
        latest(sender_psid,0);            
    }

    if(received_message.text == 'INDIA'){
        india(sender_psid,0);            
    }

    if(received_message.text == 'ENTERTAINMENT'){
        entertainment(sender_psid,0);            
    }

    if(received_message.text == 'CRICKET'){
        cricket(sender_psid,0);            
    }

    if(received_message.text== 'PHOTOS'){
        photos(sender_psid,0);            
    }

    if(received_message.text== 'VIDEOS'){
        videos(sender_psid,0);            
    }

    if(received_message.text== 'JOB'){
        jobs(sender_psid,0);            
    }

    if(received_message.text== 'ASTROLOGY'){
        astrology(sender_psid,0);            
    }

    if(received_message.text== 'KAVYA'){
        kavya(sender_psid);            
    }    

    if(received_message.text== 'E-paper'){
        e_paper(sender_psid);;            
    }
    // else{
    //     search_news(sender_psid,received_message.text);
    // }

    
    }

}